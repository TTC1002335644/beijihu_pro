define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    require(['backend/home/menu' , 'backend/home/menuAjax'], function(initMenu , saveData){
        var api = {
            index_url : 'home/web_menu/getList' + location.search,
            add_url: 'home/web_menu/addMenu',
        };
        initMenu(api.index_url);


        saveData(api.add_url);
    });

    // var Controller = {
    //     index: function () {
    //         // 初始化表格参数配置
    //         Table.api.init({
    //             extend: {
    //                 index_url: 'home/web_menu/index' + location.search,
    //                 add_url: 'home/web_menu/add',
    //                 edit_url: 'home/web_menu/edit',
    //                 del_url: 'home/web_menu/del',
    //                 multi_url: 'home/web_menu/multi',
    //                 table: 'web_menu',
    //             }
    //         });
    //
    //
    //         var table = $("#table");
    //
    //
    //         // 初始化表格
    //         table.bootstrapTable({
    //             url: $.fn.bootstrapTable.defaults.extend.index_url,
    //             pk: 'web_menu_id',
    //             sortName: 'web_menu_id',
    //             columns: [
    //                 [
    //                     {checkbox: true},
    //                     {field: 'web_menu_id', title: __('Web_menu_id')},
    //                     {field: 'language_id', title: __('Language_id')},
    //                     {field: 'name', title: __('Name')},
    //                     {field: 'pid', title: __('Pid')},
    //                     {field: 'url', title: __('Url'), formatter: Table.api.formatter.url},
    //                     {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
    //                 ]
    //             ]
    //         });
    //
    //         // 为表格绑定事件
    //         Table.api.bindevent(table);
    //     },
    //     add: function () {
    //         Controller.api.bindevent();
    //     },
    //     edit: function () {
    //         Controller.api.bindevent();
    //     },
    //     api: {
    //         bindevent: function () {
    //             Form.api.bindevent($("form[role=form]"));
    //         }
    //     }
    // };
    var Controller = {};
    return Controller;
});