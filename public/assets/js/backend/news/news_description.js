define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'news/news_description/index' + location.search,
                    add_url: 'news/news_description/add',
                    edit_url: 'news/news_description/edit',
                    del_url: 'news/news_description/del',
                    multi_url: 'news/news_description/multi',
                    table: 'news_description',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'description_id',
                sortName: 'description_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'description_id', title: __('Description_id')},
                        {field: 'news_id', title: __('News_id')},
                        {field: 'seo_title', title: __('Seo_title')},
                        {field: 'seo_keywork', title: __('Seo_keywork')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});