var comment_url = {
    commentUrl : '/saveComment',
};


$("#sendComment").click(function(){
    var data = {
        name : $("#name").val(),
        contact : $("#contact").val(),
        content : $("#content").val(),
        experience : $("#experience").val(),
    };
    $.post(comment_url.commentUrl , data , function(result){
        if(result.code == 1){
            layer.alert("提交成功", {
                icon: 1,
                skin: 'layer-ext-moon'
            },function(){
                parent.layer.closeAll();
            });
        }else{
            layer.alert("提交失败", {
                icon: 2,
                skin: 'layer-ext-moon'
            });
        }
    });
});

