var home_index_url = {
    newsLists : '/home/newsList',
    bannerLists : '/home/bannerList',
    newsDetail : 'newsDetail',
};
console.log(home_index_url);
/*获取新闻列表*/
$.get(home_index_url.newsLists , function(result){
    console.log(result.code);
    if(result.code == 1){
        var strArr = [];
        $.each(result.data ,function(k,v){
            let str =  `<div class="newstitem wow fadeOnLeft"><div class="newscontent"><a class="newsinfo fl" target="_blank" href="${home_index_url.newsDetail + '.html?news_id=' + v.news_id }" title="<strong>${v.title }</strong>" ><div class="newsbody fl" data-wow-delay=".1s"><p class="title ellipsis"><strong>${v.title}</strong></p><p class="description">${v.introduction}</p></div><div class="newsdate fr" data-wow-delay=".1.5s"><p class="md">${v.showtime}</p></div></a><i class="fa fa-angle-right fr"></i></div>
</div>`;
            if(k < 3){
                $('#newslist1').append(str);
            }else{
                $('#newslist2').append(str);
            }

        });

        console.log(strArr);
    }
});