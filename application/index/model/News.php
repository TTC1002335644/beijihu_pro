<?php
namespace app\index\model;

use app\index\model\NewsDescription;

class News extends \think\Model{
    // 表名
    protected $name = 'news';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    const IS_EXTERNAL_TRUE = 1;//有外链
    const IS_EXTERNAL_FALSE = 2;//无外链

    const IS_OPEN_TRUE = 1;//可见
    const IS_OPEN_FALSE = 2;//不可见

    /**
     * 获取关联内容
     * @return \think\model\relation\HasOne
     */
    public function hasOneNewsDescription(){
        return $this->hasOne(NewsDescription::class , 'news_id' , 'news_id');
    }


}