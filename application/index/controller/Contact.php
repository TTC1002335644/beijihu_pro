<?php
namespace app\index\controller;
use think\Controller;

class Contact extends Controller{

    public function getDetail(int $language_id = 0){
        $res = db('contact')->where(['language_id' => $language_id])->find();
        return $res;
    }

}