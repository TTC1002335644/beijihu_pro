<?php
namespace app\index\controller;

use think\Controller;
use app\admin\model\home\Banner as BannerModel;

class Banner extends Controller{

    /**
     * 获取banner列表
     * @param int $language_id
     * @return mixed
     */
    public function getBannerList(){
        $res = db('banner')->order('sort' , 'asc')->select();
        return $res;
    }

}