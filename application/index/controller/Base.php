<?php
namespace app\index\controller;

use app\admin\model\language\Language;
use app\common\controller\Frontend;

class Base extends Frontend{

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _initialize()
    {
        parent::_initialize();
        $languageList = (new Language())->getLanguageList();
        $menuList = (new WebMenu())->getMenuList($this->paramLanguageId);
//        dump($menuList);
        $this->view->assign('languageList' , $languageList);
        $this->view->assign('languageId' , $this->paramLanguageId);
        $this->view->assign('lang' , $this->paramLanguageCode);
        $this->view->assign('menuList' , $menuList);
    }

}