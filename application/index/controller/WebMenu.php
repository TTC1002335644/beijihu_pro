<?php
namespace app\index\controller;

use think\Controller;
use think\Request;
use app\admin\model\home\WebMenu as WebMenuModel;

class WebMenu extends Controller {


    /**
     * 获取菜单
     * @param int $languageId
     * @return array|false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getMenuList(int $languageId = 0){
        $list = (new WebMenuModel())
            ->where('language_id' , $languageId)
            ->order('web_menu_id', 'asc')
            ->select();
        $list = collection($list)->toArray();
        $list = Tree($list,'web_menu_id' , 'pid' , 'child');
        return $list;
    }


}