<?php

namespace app\index\controller;

use think\Request;
use app\index\model\Comment as CommentModel;


class Comment extends Base{
    protected $noNeedLogin = ['*'];

    /**
     * @return string
     * @throws \think\Exception
     */
    public function index(){
        return $this->view->fetch();
    }

    /**
     * 保存留言
     * @return \think\response\Json
     */
    public function savecomment(){
        $input = input();
        if(empty($input)){
            return json([
                'code' => 0,
            ]);
        }
        $data['name'] = isset($input['name']) ? $input['name'] : '';
        $data['contact'] = isset($input['contact']) ? $input['contact'] : '';
        $data['content'] = isset($input['content']) ? $input['content'] : '';
        $data['experience'] = isset($input['experience']) ? $input['experience'] : '';
        $field = ['name' , 'contact' , 'content' ,'experience'];
        $res = (new CommentModel())->allowField(true)->save($input);
        if($res !== false){
            return json([
                'code' => 1,
            ]);
        }else{
            return json([
                'code' => 0,
            ]);
        }

    }

}