<?php
namespace app\index\controller;

use app\index\model\News as NewsModel;
use think\Request;

class News extends Base{

    protected $noNeedLogin = ['*'];


    /**
     * 首页的新闻列表
     * @param int $size
     * @param bool $isApi
     * @return false|\PDOStatement|string|\think\Collection|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function homeNewsLists($size = 6 , $isApi = true){
        $field = ['news_id' , 'title' , 'introduction' , 'showtime'];
        $condition = [  'is_open' => NewsModel::IS_OPEN_TRUE];
        $lists = (new NewsModel)->field($field)->where($condition)->order('news_id','desc')->page(1,$size)->select();
        if(!empty($lists)){
            foreach ($lists as & $v){
                $v->showtime = date('m-d');
            }
        }
        if(!$isApi){
            return  $lists;
        }
        return json([
            'code' => 1,
            'data' => $lists
        ]);
    }

    /**
     * 新闻版面
     * @param int $pageSize
     * @param int $pageNow
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function newsLists(int $column_type = 1 ,int $pageSize = 15 , int $pageNow = 1){
        $condition = [
            'is_open' => NewsModel::IS_OPEN_TRUE,
            'column_type' =>$column_type
        ];
        $newsLists = (new NewsModel())
            ->where($condition)
            ->paginate($pageSize);
        $this->view->assign('newsLists' , $newsLists);
        $this->view->assign('column_type' , $column_type);
        return $this->view->fetch();
    }
    /**
     * 新闻详情
     * @param int $news_id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function newsDetail($news_id = 0){
        $newsDetail = (new NewsModel())
            ->where(['news_id' => $news_id , 'is_open' => NewsModel::IS_OPEN_TRUE])
            ->with([
                'hasOneNewsDescription'
            ])
            ->find();

        $column_type = '';
        if(!empty($newsDetail)){
            $column_type = __("Column_type ".$newsDetail->column_type);
        }
        $this->assign('news' , $newsDetail);
        $this->assign('column_type' , $column_type);
        return view();
    }

    /**
     * 获取上一张和下一张
     * @param int $news_id
     * @param int $language_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getPrevNextNews(int $news_id = 0 , int $language_id = 1 , string $lang){
        $condition = [ 'language_id' => $language_id , 'is_open' => NewsModel::IS_OPEN_TRUE];
        $field = ['news_id' , 'title' , 'showtime' ,'is_external' , 'external_url'];
        $prevData = (new NewsModel)->where($condition)->where('news_id' , '<' , $news_id)->order('news_id','desc')->field($field)->find();
        $nextData = (new NewsModel)->where($condition)->where('news_id' , '>' , $news_id)->order('news_id','asc')->field($field)->find();
        if(!empty($prevData)){
            $prev = [
                'title' => $prevData->title,
                'url' => ($prevData->is_external == NewsModel::IS_OPEN_TRUE) ? $prevData->external_url :'./news_detail.html?lang='.$lang.'&news_id='.$prevData->news_id,
            ];
        }else{
            $prev = [
                'title' => __('None'),
                'url' => '#',
            ];
        }

        if(!empty($nextData)){
            $next = [
                'title' => $nextData->title,
                'url' => ($nextData->is_external == NewsModel::IS_OPEN_TRUE) ? $nextData->external_url : './news_detail.html?lang='.$lang.'&news_id='.$nextData->news_id,
            ];
        }else{
            $next = [
                'title' =>__('None'),
                'url' => '#',
            ];
        }
        return [
            'prev' => $prev,
            'next' => $next,
        ];
    }

}