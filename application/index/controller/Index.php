<?php

namespace app\index\controller;

//use app\common\controller\Frontend;

use app\index\model\News as NewsModel;

class Index extends Base {

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index(){
        //获取Banner
        $newsLists = (new News())->homeNewsLists(6,false);
        $bannerLists = (new Banner())->getBannerList();
        $this->view->assign('bannerLists' , $bannerLists);
        $this->view->assign('newsLists' , $newsLists);
        return $this->view->fetch();
    }


}
