<?php

use \think\Route;

Route::get('comment','index/Comment/index');//在线留言

Route::post('saveComment','index/Comment/savecomment');//在线留言

Route::get('home/newsList','index/News/homeNewsLists');//首页新闻列表

Route::get('newsLists','index/News/newsLists');//新闻列表

Route::get('index/newsDetail','index/News/newsDetail');//新闻详情


//Route::group('',function(){
//    Route::group('canjuleimu' ,function(){
//        Route::get('index' , 'index/canjuleimu/index');
//    });
//    Route::group('chenggonganli' ,function(){
//        Route::get('shejianli/index' , 'index/chenggonganli/shejianli_index');
//        Route::get('yunyinganli/index' , 'index/chenggonganli/yunyinganli_ndex');
//        Route::get('yunyinganli/list_18_1' , 'index/chenggonganli/yunyinganlilist_18_1');
//        Route::get('yunyinganli/list_18_2' , 'index/chenggonganli/yunyinganlilist_18_2');
//    });
//    Route::group('dianqileimu' ,function(){
//        Route::get('index' , 'index/dianqileimu/index');
//    });
//    Route::group('dianshangyunying' ,function(){
//        Route::get('pinpaiyingxiaocehua/index' , 'index/dianshangyunying/dianshangyunying_index');
//        Route::get('xinmeitiyunying/index' , 'index/dianshangyunying/xinmeitiyunying_index');
//        Route::get('zhengdianyunyingtuoguan/index' , 'index/dianshangyunying/zhengdianyunyingtuoguan_index');
//        Route::get('zhitongchetuoguan/index' , 'index/dianshangyunying/zhitongchetuoguan_index');
//    });
//    Route::group('gengduofuwu' ,function(){
//        Route::get('index' , 'index/gengduofuwu/index');
//    });
//    Route::group('guanyuwomen' ,function(){
//        Route::get('hexintuandui/index' , 'index/guanyuwomen/hexintuandui_index');
//        Route::get('jiaruwomen/index' , 'index/guanyuwomen/jiaruwomen_index');
//        Route::get('jintaomingyu/index' , 'index/guanyuwomen/jintaomingyu_index');
//        Route::get('lianxiwomen/index' , 'index/guanyuwomen/lianxiwomen_index');
//        Route::get('qiyegaikuang/index' , 'index/guanyuwomen/qiyegaikuang_index');
//        Route::get('qiyewenhua/index' , 'index/guanyuwomen/qiyewenhua_index');
//    });
//    Route::group('huwaileimu' ,function(){
//        Route::get('index' , 'index/huwaileimu/index');
//    });
//    Route::group('jiajileimu' ,function(){
//        Route::get('index' , 'index/jiajileimu/index');
//    });
//    Route::group('jiajuleimu' ,function(){
//        Route::get('index' , 'index/jiajuleimu/index');
//    });
//    Route::group('jichuyouhua' ,function(){
//        Route::get('index' , 'index/jichuyouhua/index');
//    });
//    Route::group('jintaozhinang' ,function(){
//        Route::get('index' , 'index/jintaozhinang/index');
//        Route::get('xianshangfenxiang/index' , 'index/jintaozhinang/xianshangfenxiang_index');
//        Route::get('xianxiafenxiang/index' , 'index/jintaozhinang/xianxiafenxiang_index');
//        Route::get('yunyingwenku/index' , 'index/jintaozhinang/yunyingwenku_index');
//    });
//    Route::group('kefuwaibao' ,function(){
//        Route::get('index' , 'index/kefuwaibao/index');
//    });
//    Route::group('lubofenxiang' ,function(){
//        Route::get('index' , 'index/lubofenxiang/index');
//    });
//    Route::group('meishileimu' ,function(){
//        Route::get('index' , 'index/meishileimu/index');
//    });
//    Route::group('meizhuangleimu' ,function(){
//        Route::get('index' , 'index/meizhuangleimu/index');
//    });
//    Route::group('muyingleimu' ,function(){
//        Route::get('index' , 'index/muyingleimu/index');
//    });
//    Route::group('nanzhuangleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('nvzhuangleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('peishileimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('qianniuzhibo' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('qicheyongpin' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('shijuesheji' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('shipinleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('shumajiadian' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('tongzhuangleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('tushuleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('wangjidabaofa' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('wenjuxiangqing' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//
//    Route::group('wujinleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('xiangbaoleimu' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('xinwenzhongxin' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('zhengdianyunying' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//    Route::group('zhibofenxiang' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//
//    Route::group('zhitongche' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//
//    Route::group('zuanzhanliuliang' ,function(){
//        Route::get('index' , 'index/index/index');
//    });
//
//});









Route::get('login','/admin/index/login');


return [
    //别名配置,别名只能是映射到控制器且访问时必须加上请求的方法
    '__alias__'   => [
    ],
    //变量规则
    '__pattern__' => [
    ],
//        域名绑定到模块
//        '__domain__'  => [
//            'admin' => 'admin',
//            'api'   => 'api',
//        ],
];
