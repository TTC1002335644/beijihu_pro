<?php

return [
    'Comment'     => 'ID',
    'Language_id' => '语言',
    'Name'        => '姓名',
    'Contact'     => '联系方式',
    'Content'     => '反馈内容',
    'Experience'     => '是否有经验',
    'Createtime'     => '反馈时间',
];
