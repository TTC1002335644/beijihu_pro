<?php

return [
    'Language_id' => '语言ID',
    'Status'      => '状态',
    'Status 1'    => '正常',
    'Status 2'    => '关闭',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
