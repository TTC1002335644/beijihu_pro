<?php

return [
    'News_id'         => '新闻ID',
    'Language_id'      => '语言ID',
    'News_category_id' => '所属分类',
    'Title'            => '新闻标题',
    'Introduction'     => '新闻简介',
    'Image'            => '封面',
    'Author'           => '作者',
    'Is_open'          => '状态',
    'Is_open 1'        => '正常',
    'Is_open 2'        => '失效',
    'Is_external'      => '是否跳转外链',
    'Is_external 1'    => '跳转',
    'Is_external 2'    => '不跳转',
    'Column_type'      => '新闻栏目',
    'Column_type 1'    => '新闻动态',
    'Column_type 2'    => '媒体报道',
    'Column_type 3'    => '员工风采',
    'Column_type 4'    => '精彩视频',
    'External_url'     => '外链链接',
    'Showtime'         => '新闻时间',
    'Createtime'       => '上传时间',
    'Updatetime'       => '修改时间',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO描述',
];
